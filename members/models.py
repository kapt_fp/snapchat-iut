# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Member(models.Model):
    user = models.OneToOneField(User, verbose_name='User')
    friends = models.ManyToManyField("self", related_name="+", verbose_name='Friends', blank=True)
    friend_requests = models.ManyToManyField("self",
                                             related_name="related_friend_requests",
                                             symmetrical=False,
                                             verbose_name='Friend requests',
                                             blank=True)

    class Meta:
        verbose_name = 'Member'
        verbose_name_plural = 'Members'

# Create your models here.
    def __str__(self):
        return self.user.username
