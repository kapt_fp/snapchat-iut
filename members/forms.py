# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django import forms
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm
from django.contrib.auth.models import User


class AuthenticationForm(DjangoAuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur",
                               max_length=254,
                               widget=forms.TextInput(attrs={"class": "form-control",
                                                             "placeholder": "Nom d'utilisateur"}))
    password = forms.CharField(label="Mot de passe",
                               widget=forms.PasswordInput(attrs={"class": "form-control",
                                                                 "placeholder": "Mot de passe"}))


class RegistrationForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur",
                               max_length=254,
                               widget=forms.TextInput(attrs={"class": "form-control",
                                                             "placeholder": "Nom d'utilisateur"}))

    email = forms.EmailField(label="Email",
                             widget=forms.TextInput(attrs={"class": "form-control",
                                                           "placeholder": "Email",
                                                           "type": "email"}))

    password = forms.CharField(label="Mot de passe",
                               widget=forms.PasswordInput(attrs={"class": "form-control",
                                                                 "placeholder": "Mot de passe"}))

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Un utilisateur avec l'email {} existe déjà, veuillez vous connecter".format(email))
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("Un utilisateur avec le username {} existe déjà, veuillez vous connecter".format(username))
        return username

    def clean_password(self):
        password = self.cleaned_data['password']
        min_length = 7
        if len(password) < min_length:
            raise forms.ValidationError("trop court, le minimum est {}".format(min_length))
        return password
